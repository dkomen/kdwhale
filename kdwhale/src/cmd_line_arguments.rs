/*
    kdwhale Copyright (C) 2020  Dimension15
    Authors {
      {"Name": "Dean Komen", "Contact": "www.dimension15.co.za"}      
    }

    This program is free software: you can redistribute it and/or modify
    it under the terms of version 1 of the Dimension 15 (PTY) LTD General
    Public License as published by Dimension 15 (PTY) LTD.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 1
    of the Dimension 15 (PTY) LTD General Public License for more details.

    You should have received a copy of the  Dimension 15 (PTY) LTD 
    General Public License along with this program. If not, 
    see <https://www.dimension15.co.za> under the licenses section.
*/
use std::env;

pub struct Argument{
    pub index: usize,
    pub name: String,
    pub value: String
}

//Get arguments from command line
pub fn get_arguments() -> Result<Vec<Argument>,()> {            
    let args_array: Vec<String> = env::args().collect::<Vec<String>>();
    let arguments: Result<Vec<Argument>, ()> = get_settings(args_array);
    
    arguments
}

//If an argument exists on the command line then add it and its value to Argument Vec to return
fn get_settings(args_array: Vec<String>) -> Result<Vec<Argument>, ()> {    
    let mut arguments_to_return: Vec<Argument> = vec!();

    if args_array.len() == 1 {    
        println!("{}", get_help_screen_text());    
        return Err(())
    }
    

    let mut index_i_exists: bool = false;
    let mut index_ie_exists: bool = false;
    let mut index_o_exists: bool = false;
    let mut index_x_exists: bool = false;
    let mut index_p_exists: bool = false;
    let mut index_r_exists: bool = false;

    let index_h = get_index_of_arg("-h", &args_array); //Help screen
    let index_i = get_index_of_arg("-i", &args_array); //Input file
    let index_ie = get_index_of_arg("-e", &args_array); //Input file to embed
    let index_o = get_index_of_arg("-o", &args_array); //Output file
    let index_x = get_index_of_arg("-x", &args_array); //Output file
    let index_p = get_index_of_arg("-p", &args_array); //password
    let index_s = get_index_of_arg("-s", &args_array); //Smile screen    
    let index_r = get_index_of_arg("-r", &args_array); //encrytion rounds
    
    if index_h.index > 0 {
        println!("{}", get_help_screen_text());
        return Err(());
    } else {
        if index_s.index > 0 {
            println!("{}", get_smile_screen_text());
        };
        if index_i.index > 0 {
            index_i_exists = true;
            arguments_to_return.push(index_i);        
        };        
        if index_ie.index > 0 {
            index_ie_exists = true;
            arguments_to_return.push(index_ie);        
        };
        if index_o.index > 0 {
            index_o_exists = true;
            arguments_to_return.push(index_o);        
        };
        if index_x.index > 0 {
            index_x_exists = true;
            arguments_to_return.push(index_x);        
        };        
        if index_p.index > 0 {
            index_p_exists = true;
            arguments_to_return.push(index_p);        
        }; 
        if index_r.index > 0 {
            index_r_exists = true;            
            arguments_to_return.push(index_r);        
            if get_argument_value("-r", &arguments_to_return).unwrap().parse::<u32>().unwrap() > 100 {
                println!("Argument '-r' must be a whole number (integer) from 1 to 100 inclusively");
                return Err(());  
            }
        };          
    }
    if index_i_exists && !index_x_exists {
        if !index_o_exists || !index_ie_exists{
            println!("If argument '-i' is given then arguments '-o' and '-e' must also be given");
            return Err(());
        }
    }
    if index_i_exists && index_o_exists{
        if !index_ie_exists && !index_x_exists {
            println!("If argument '-i' and '-o' are given then arguments '-e' or '-x' must also be given");
            return Err(());
        }
    }
    if index_o_exists {
        if !index_i_exists && !index_x_exists{
            println!("If argument '-o' is given then arguments '-x' and '-i' and optionally '-e' must also be given");
            return Err(());
        }
    } 
    if index_ie_exists {
        if !index_i_exists || !index_o_exists {
            println!("If argument '-e' is given then arguments '-i' and '-o' must also be given");
            return Err(());
        }
    };
    if index_x_exists {
        if index_i_exists {
            println!("If argument '-x' is given then arguments '-i' may not be given");
            return Err(());
        }
    };

    Ok(arguments_to_return)
}

//Get the index of the argument we want in the command line argments array 
fn get_index_of_arg(arg_id: &str, args_array: &Vec<String>) -> Argument {
    let arg_index = args_array.iter().position(|arg| arg.starts_with(arg_id)).unwrap_or(0);    
    let mut argument_data = Argument {
        index: arg_index,
        name: arg_id.to_owned(),
        value: String::new()
    };

    if arg_index >= 1 {        
        let result = get_arg_value(arg_index, args_array);
        if result.is_ok() {
            argument_data.value=result.unwrap();
        }
    }

    argument_data
}

fn get_arg_value(index: usize, arg_array: &Vec<String>) -> Result<String,()> {
    if arg_array.len() > index + 1 {
        let next_value = arg_array[index + 1].to_owned();
        if next_value.starts_with("-") {
            Ok(String::new())
        } else {            
            Ok(next_value)
        }
    } else {
        Err(())
    }
}

//Get the value of an argument if it was supplied on the command line
pub fn get_argument_value(argument_name: &str, arguments: &Vec<Argument>) -> Option<String> {
    for argument in arguments {
        if argument.name == argument_name {
            return Some(argument.value.to_owned());
        }
    };
    None
}

//Get the value of an argument if it was supplied on the command line
pub fn argument_exists(argument_name: &str, arguments: &Vec<Argument>) -> bool {
    for argument in arguments {
        if argument.name == argument_name {
            return true
        }
    };
    
    false
}


fn get_help_screen_text() -> String {
    let mut help_screen_text = format!("kdwhale version: 0.1.0");
    help_screen_text = help_screen_text + "\n\nEmbed any small file inside a png image file - Steganography";    
    help_screen_text = help_screen_text + "\nWritten by Dean Komen 2020";
    help_screen_text = help_screen_text + "\n\nLICENSE: Dimension 15 General Public License V1 <https://www.dimension15.co.za>";
    help_screen_text = help_screen_text + "\nSource code available at: https://www.gitlab.com/dkomen/kdwhale";
    help_screen_text = help_screen_text + "\nNOTE: All paths supplied must be inside double quotes if there are spaces in the path - \"\"";
    help_screen_text = help_screen_text + "\n\nCommand-line arguments:";
    help_screen_text = help_screen_text + "\n    -h    : 'Help': Display this help screen";
    help_screen_text = help_screen_text + "\n    -i    : 'Input image file': The full path to the source file that a copy must be made of and the file specified by";
    help_screen_text = help_screen_text + "\n            the (-e) argument will be hidden inside";
    help_screen_text = help_screen_text + "\n    -e   : 'Input file to hide (embed)': The full path to the file containing the data to hide";
    help_screen_text = help_screen_text + "\n            The file name component of the path may not exceed 75 characters";
    help_screen_text = help_screen_text + "\n    -o    : 'Output file': The output file containing the result of the hidden file or the result of the extracted file if using (-x)";
    help_screen_text = help_screen_text + "\n    -x    : 'Extract hiddent file': Extract the file hidden inside the file specified by the argument (-i) to the file specified with argument (-o)";
    help_screen_text = help_screen_text + "\n    -p    : 'Password': Add an alphanumeric character password of inclusively between 10 and 100 characters";
    help_screen_text = help_screen_text + "\n          : If you only specify the (-p) argument then once the program executes you will be asked for a password";    
    help_screen_text = help_screen_text + "\n    -r    : Number of additional rounds of encryption. The more rounds the stronger and slower the encryption.";
    help_screen_text = help_screen_text + "\n            If you elect to specify the number of rounds then that value must be remembered along with the password if one";
    help_screen_text = help_screen_text + "\n            is supplied. It is the combination of any password and rounds specified that controls how the encryption is done.";
    help_screen_text = help_screen_text + "\n            Allowed numbers are unsigned integer values from 1 to 100";
    help_screen_text = help_screen_text + "\n            If (-r) is NOT supplied at all and no password is supplied either then the default value will be 25, however, if a password ";
    help_screen_text = help_screen_text + "\n            is supplied then the number of rounds is the number of characters in your password up to a maximum of 100";    
    // help_screen_text = help_screen_text + "\n            If argument (-o) is not supplied then the original hidden file name is used and extracted into the current directory";
    // help_screen_text = help_screen_text + "\n            If argument (-o) is also given then extracted file will named with this full path (if it includes";
    // help_screen_text = help_screen_text + "\n            a file name too, else the original file name of the hidden file will be used";
    //help_screen_text = help_screen_text + "\n    -s    : To see a lovely smile";
    help_screen_text = help_screen_text + "\n\nExamples";
    help_screen_text = help_screen_text + "\n\nThese examples show the use of Linux path names, on Windows use Windows style path names, like: \"c:\\MyPhotos\"";
    help_screen_text = help_screen_text + "\n    1. Hide file HideData.jpg in file named Original.png and save the new file as New.png";    
    help_screen_text = help_screen_text + "\n       kdwhale -i \"/home/dean/Pictures/Original.png\" -e \"/home/dean/Pictures/HideData.jpg\" -o \"/home/dean/Pictures/New.png\"";
    help_screen_text = help_screen_text + "\n    2. Hide text file HideData.txt in file named Original.png and save the new file as New.png";    
    help_screen_text = help_screen_text + "\n       kdwhale -i \"/home/dean/Pictures/Original.png\" -e \"/home/dean/Pictures/HideData.txt\" -o \"/home/dean/Pictures/New.png\"";
    help_screen_text = help_screen_text + "\n    3. Using a password hide a pdf file HideData.pdf in file named Original.jpg and save the new file as New.png";    
    help_screen_text = help_screen_text + "\n       kdwhale -p sectret75325pOss -i \"/home/dean/Pictures/Original.jpg\" -e \"/home/dean/Pictures/HideData.pdf\" -o \"/home/dean/Pictures/New.png\"";
    help_screen_text = help_screen_text + "\n    4. Using a password that (never displayed and asked for when the program is run) hides a pdf file HideData.pdf in file named Original.jpg and save";
    help_screen_text = help_screen_text + "\n       the new file as New.png";
    help_screen_text = help_screen_text + "\n       kdwhale -p -i \"/home/dean/Pictures/Original.jpg\" -e \"/home/dean/Pictures/HideData.pdf\" -o \"/home/dean/Pictures/New.png\"";
    help_screen_text = help_screen_text + "\n       You will be asked for your password once you execute the above command";

    // help_screen_text = help_screen_text + "\n    4. Extract hidden file to current directory using its original file name";    
    // help_screen_text = help_screen_text + "\n       kdwhale -x -i \"/home/dean/Pictures/New.png\"";
    // help_screen_text = help_screen_text + "\n    5. Extract hidden file to new directory but keep its original name";    
    // help_screen_text = help_screen_text + "\n       kdwhale -x -i \"/home/dean/Pictures/New.png\" -i \"/home/dean/Pictures/ExtractedDataFiles\"";
    help_screen_text = help_screen_text + "\n    5. Extract a hidden file, that is password protected with the original file name into the current directory";    
    help_screen_text = help_screen_text + "\n       kdwhale -x \"/home/dean/Pictures/New.png\" -p";    
    help_screen_text = help_screen_text + "\n    6. Extract a hidden file with the original file name into the current directory, but with a supplied password and 150 rounds of encryption";    
    help_screen_text = help_screen_text + "\n       kdwhale -x \"/home/dean/Pictures/New.png\" -p secretpassword12345 -r 150";    
    help_screen_text = help_screen_text + "\n    7. Extract a hidden pdf file using a required alphanumeric character password and giving the extracted file a new name";    
    help_screen_text = help_screen_text + "\n       kdwhale -x -p sectret75325pOss -i \"/home/dean/Pictures/New.png\" -o \"/home/dean/Pictures/ExtractedDataFiles/ExtractedData.pdf\"";    
    help_screen_text = help_screen_text + "\n    8. Extract a hidden pdf file (using an alphanumeric character password which is asked for at runtime as a secret) and giving the extracted";
    help_screen_text = help_screen_text + "\n       file a new name";  
    help_screen_text = help_screen_text + "\n       kdwhale -p -x \"/home/dean/Pictures/New.png\" -o \"/home/dean/Pictures/ExtractedDataFiles/ExtractedData.pdf\"";    
    help_screen_text = help_screen_text + "\n";
    help_screen_text = help_screen_text + "\n     A typical Linux example:";
    help_screen_text = help_screen_text + "\n       Hide data where all the files are in the current directory";    
    help_screen_text = help_screen_text + "\n          ./kdwhale -p -i Original.png -e MySecrets.txt -o Secrets.png -r 50";    
    help_screen_text = help_screen_text + "\n       Extract your secret file into the current folder";    
    help_screen_text = help_screen_text + "\n          ./kdwhale -p -x Original.png -r 50";    
    
    help_screen_text = help_screen_text + "\n     A typical Windows example:";
    help_screen_text = help_screen_text + "\n       Hide data where all the files are in the current directory";    
    help_screen_text = help_screen_text + "\n          kdwhale -p -i Original.png -e MySecrets.txt -o Secrets.png -r 50";    
    help_screen_text = help_screen_text + "\n       Extract your secret file into the current folder";    
    help_screen_text = help_screen_text + "\n          kdwhale -p -x Original.png -r 50";    
    help_screen_text = help_screen_text + "\n\n\n";
    help_screen_text
}

fn get_smile_screen_text() -> String {
    let mut help_screen_text = format!("\n");
    help_screen_text = help_screen_text + "\n    (ʘ‿ʘ)";
    help_screen_text = help_screen_text + "\n";
    help_screen_text = help_screen_text + "\n\n\n";
    help_screen_text
}