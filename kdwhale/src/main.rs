/*
    kdwhale Copyright (C) 2020  Dimension15
    Authors {
      {"Name": "Dean Komen", "Contact": "www.dimension15.co.za"}      
    }

    This program is free software: you can redistribute it and/or modify
    it under the terms of version 1 of the Dimension 15 (PTY) LTD General
    Public License as published by Dimension 15 (PTY) LTD.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 1
    of the Dimension 15 (PTY) LTD General Public License for more details.

    You should have received a copy of the  Dimension 15 (PTY) LTD 
    General Public License along with this program. If not, 
    see <https://www.dimension15.co.za> under the licenses section.
*/
extern crate image;
use std::env;
use std::io::prelude::*;
use std::fs::File;
pub mod cmd_line_arguments;

const MAX_LENGTH_DATA: u32 = 10*8;
const MAX_LENGTH_FILE_NAME: u32 = 75*8;

//Get a particular bit and return true if its 1 and false if its 0
fn get_bit_at(input: u32, n: u8) -> bool {
    if n < 32 {
        input & (1 << n) != 0
    } else {
        false
    }
}

//Convert a byte array to an array of booleans for each bit
fn extract_bits(string: &[u8]) -> Vec<bool> {
    let mut bits: Vec<bool> = vec!();
    for byte in string {
        for i in 0..8 {
            bits.push(get_bit_at(*byte as u32, i))
        }
    }
    
    bits
}

fn main() {
    //*************************************
    //Get arguments
    let result_arguments =  cmd_line_arguments::get_arguments();
    if !result_arguments.is_ok() {
        println!("-h for help");
        std::process::exit(1);
    }

    //*************************************
    //Check arguments
    let arguments: Vec<cmd_line_arguments::Argument> = result_arguments.unwrap();
    if cmd_line_arguments::argument_exists("-i", &arguments) == false && cmd_line_arguments::argument_exists("-x", &arguments) == false {
        println!("At least agrument (-i) or (-x) must be supplied");
        println!("-h for help");
        std::process::exit(1);
    }

    //Is extraction set
    let extraction = cmd_line_arguments::argument_exists("-x", &arguments);
    
    // Open input file        
    let mut input_file_name = String::new();
    
    if extraction {
        input_file_name = cmd_line_arguments::get_argument_value("-x", &arguments).unwrap();
        let input_file_name_result = std::fs::File::open(&input_file_name);
        if input_file_name_result.is_err()
        {
            println!("Input file could not be read (-x): {}", &input_file_name);
            println!("-h for help");
            //std::process::exit(1);
        }
    } else {
        input_file_name = cmd_line_arguments::get_argument_value("-i", &arguments).unwrap();
        let input_file_name_result = std::fs::File::open(&input_file_name);
        if input_file_name_result.is_err()
        {
            println!("Input file could not be read (-i): {}", &input_file_name);
            println!("-h for help");
            std::process::exit(1);
        }
    }

    //Output file name
    let mut output_file_name = String::new();
    if cmd_line_arguments::argument_exists("-o", &arguments) {
        output_file_name = cmd_line_arguments::get_argument_value("-o", &arguments).unwrap();
    }

    // Open input file to embed and get its bytes
    let mut input_file_to_embed_bytes: Vec<u8> = vec!();     
    if cmd_line_arguments::argument_exists("-e", &arguments) {
        let input_file_to_embed_name = cmd_line_arguments::get_argument_value("-e", &arguments).unwrap();
        let input_file_to_embed_name_result = std::fs::File::open(&input_file_to_embed_name);
        if input_file_to_embed_name_result.is_err()
        {
            println!("Input file you wish to hide could not be read (-e): {}", &input_file_to_embed_name);
            println!("-h for help");
            std::process::exit(1);
        }
        //Get input file bytes            
        let _ = input_file_to_embed_name_result.unwrap().read_to_end(&mut input_file_to_embed_bytes);
    }
    
    //End check arguments
    //*************************************

    let mut password = String::from("5R7FsjKJF3rsTcWr9t4s");
    let mut password_value: Option<String> = None;
    const BLOCK_SIZE: u32 = 225;
    let mut hash_rounds = BLOCK_SIZE;
    let r_value = cmd_line_arguments::get_argument_value("-r", &arguments);
    if  r_value == None {
        hash_rounds = BLOCK_SIZE * 25;
    } else {
        hash_rounds = BLOCK_SIZE * r_value.unwrap().parse::<u32>().unwrap();
    }
    
    if cmd_line_arguments::argument_exists("-p", &arguments) {
        password_value = cmd_line_arguments::get_argument_value("-p", &arguments);
        //println!("Password value is: '{}'", password_value.unwrap());
        if password_value == None || password_value.unwrap().len()==0 {
            use rpassword;
            println!("Please enter an alphanumeric password (10 to 100 chars): ");
            let result_password = rpassword::read_password();
            if result_password.is_ok() {
                password = result_password.unwrap();

                if password.len() < 10 || password.len() > 100 {
                    println!("Password must be inclusively between 10 and 100 alphanumeric characters (-p)");
                    println!("-h for help");
                    std::process::exit(0);    
                }

                if cmd_line_arguments::argument_exists("-e", &arguments) {
                    println!("Please retype your password again: ");
                    let result_password_verify = rpassword::read_password();
                    if result_password_verify.is_ok() {                    
                        let password_verify = result_password_verify.unwrap();                                        
                        if password != password_verify {
                            println!("Passwords do not match! Can not continue.");
                            std::process::exit(0);
                        }
                    } else {
                        println!("Some unknown verification password error");
                        std::process::exit(0);
                    }
                }
                if cmd_line_arguments::get_argument_value("-r", &arguments) == None {
                    hash_rounds = BLOCK_SIZE * password.len() as u32;    
                }
            }
        }                        
    }           
    let mut password: Vec<u8> = password.as_bytes().to_vec();    
    //println!("Hash_rounds: {}", hash_rounds);
    password = expand_password(&password, hash_rounds);    
    password = hash_password(&password, 0, hash_rounds * 2);   
    //println!("Done");  
    //println!("XXX{:?}XXX", password);
    if extraction {
        extract::generate_output(&input_file_name, &output_file_name, &password);
    } else {        
        
        let data_file_name = cmd_line_arguments::get_argument_value("-e", &arguments).unwrap();
        create::generate_output(&input_file_to_embed_bytes, &data_file_name, &input_file_name, &output_file_name, &password);
    }

    println!("Done!");
}

fn expand_password(password: &Vec<u8>, rounds: u32) -> Vec<u8> {
    let mut expanded_password: Vec<u8> = password.clone();
    loop {        
        for i in 0..expanded_password.len() {
            if expanded_password.len() as u32 == rounds {
                break;
            }
            let mut c = expanded_password[i].rotate_left((expanded_password[i] as u32) + i as u32 + expanded_password.len() as u32);
            for _ in 0..1000 {
                if c > 127 {
                    c = c.rotate_left(rounds);
                } else {
                    c = c.rotate_left((255-c) as u32);
                }
            }
            expanded_password.push(c);
        }
        if expanded_password.len() as u32 >= rounds {
            break;
        }
    }

    expanded_password
}

fn hash_password(password: &Vec<u8>, current_round: u32, rounds: u32) -> Vec<u8> {   
    let mut new_password_bytes: Vec<u8> = vec!();
    let mut current_byte_index = 0;
    let mut password_clone = password.clone();
    password_clone.reverse();

    for b in 0..password.len() {
        let mut f = password[b].rotate_left(password[b] as u32 + b as u32 + password_clone[b] as u32);         
        new_password_bytes.push(f);
        
        current_byte_index = current_byte_index + 1;
    }
    if current_round != rounds {
        new_password_bytes = hash_password(&new_password_bytes, current_round + 1, rounds);
    };

    new_password_bytes
}

mod extract {
    use image::{GenericImage, GenericImageView, ImageBuffer, RgbImage};
    use std::io::prelude::*;
    use std::fs::File;
    
    pub fn get_new_bit_value_from_channel(pixel_channel_value: u8) -> bool {
        return pixel_channel_value % 2 == 0; //If even number then true, else false
    }

    fn get_chars_from_bits(bits_to_extract: &Vec<bool>) -> Vec<char> {          
        let mut bit_offset: u32 = 0;
        let mut extracted_chars: Vec<char> = vec!();
        let mut extracted_char: u8 = 0;
        let mut bit_counter: u8 = 0;
        let mut char_counter: u64 = 0;
        for bit_index in 0..bits_to_extract.len() {        
            if bits_to_extract[(bit_index) as usize] {
                if bit_index % 8 > 0 {
                    extracted_char = extracted_char + (2 << (((bit_index -1) as u32) - bit_offset));
                } else {
                    extracted_char = extracted_char + 1;
                }
            }
            bit_counter = bit_counter + 1;
            if bit_counter % 8 == 0 { //&& extracted_char != 0
                extracted_chars.push(extracted_char as char);
                char_counter = char_counter + 1;
                //let U: u8 = extracted_chars[extracted_chars.len()-1] as u8;
                bit_offset = 8 + ((bit_index / 8) as u32 * 8);
                extracted_char = 0;
                bit_counter = 0;
            }
        }
        //extracted_chars.push(extracted_char as char);

        extracted_chars
    }

    fn convert_bool_array_into_number(bits_to_extract_length: &Vec<bool>) -> u64 {
        let mut bit_counter = 0;           
        let mut bit_offset: i16 = 1;
        let mut length_char: u8 = 0;
        let mut length_chars: Vec<char> = vec!();
        for bit_index in 0..bits_to_extract_length.len() {                            
            if bits_to_extract_length[(bit_index) as usize] {
                if bit_index != 0 && bit_index % 8 != 0{
                    length_char = length_char + (2 << (bit_index as i16 - bit_offset));
                } else {
                    length_char = length_char + 1;
                }
            }
            bit_counter = bit_counter + 1;
            if bit_counter % 8 == 0 && length_char != 0 {
                length_chars.push(length_char as char);
                bit_offset = (bit_offset-1) as i16 + 9;
                length_char = 0;
            }
        }
        
        length_chars.into_iter().collect::<String>().parse::<u64>().unwrap()
    }

    //Return all the first bytes that arent 0
    fn remove_zero_byte(bytes: &Vec<u8>) -> Vec<u8> {
        let mut extracted_bytes: Vec<u8> = vec!();
        for b in bytes {
            if *b != 0 {
                extracted_bytes.push(*b);
            } else {
                break;
            }
        }

        extracted_bytes
    }

    pub fn generate_output(input_file: &str, output_file: &str, password: &Vec<u8>) {
        let mut bits_to_extract: Vec<bool> = vec!();
        let mut bits_to_extract_length: Vec<bool> = vec!();
        let mut bits_to_extract_file_name: Vec<bool> = vec!();
        let mut original_file_name = String::new();
        let bits_for_password: Vec<bool> = super::extract_bits(&password);

        let mut image = image::io::Reader::open(input_file).unwrap().with_guessed_format().unwrap().decode().unwrap();       
        
        //let mut new_image = image.to_rgb();
        let mut bit_counter = 0;
        let mut length_bit_counter: u32 = 0;        
        let mut length_of_data_to_extract: u64 = 0;
        let mut pixel_counter: u64 = 0;
        let mut pixel_counter_extract: u64 = 0;
        let mut password_bit_counter: usize = 0;
        //let mut length_of_data: u64 = 0;
        for x in (0..(image.dimensions().0 -1)).step_by(1) {
            for y in (0..(image.dimensions().1 -1)).step_by(1) {
                pixel_counter = pixel_counter + 1;
                let pixel = image.get_pixel(x, y); 
                let new_bit_value_r: bool = get_new_bit_value_from_channel(pixel[0]);
                let new_bit_value_b: bool = get_new_bit_value_from_channel(pixel[2]);
                if length_bit_counter < super::MAX_LENGTH_DATA as u32 {                    
                    bits_to_extract_length.push(new_bit_value_b ^ bits_for_password[password_bit_counter]);
                    length_bit_counter = length_bit_counter + 1;
                    if length_bit_counter == super::MAX_LENGTH_DATA as u32 {
                                       
                        length_of_data_to_extract = convert_bool_array_into_number(&bits_to_extract_length);
                    }
                } else if length_bit_counter < super::MAX_LENGTH_DATA as u32 + super::MAX_LENGTH_FILE_NAME {                    
                    bits_to_extract_file_name.push(new_bit_value_b ^ bits_for_password[password_bit_counter]);
                    length_bit_counter = length_bit_counter + 1;
                    if length_bit_counter == super::MAX_LENGTH_DATA + super::MAX_LENGTH_FILE_NAME {
                        let file_name_bytes = get_chars_from_bits(&bits_to_extract_file_name).iter().map(|c| *c as u8).collect();
                        let file_name_bytes = remove_zero_byte(&file_name_bytes);
                        original_file_name = String::from_utf8(file_name_bytes).unwrap();
                    }
                } else {                    
                    bits_to_extract.push(new_bit_value_r ^ bits_for_password[password_bit_counter]);
                    bits_to_extract.push(new_bit_value_b ^ bits_for_password[password_bit_counter]);
                    bit_counter = bit_counter + 2;
                    if bit_counter % 8 == 0 {
                        bit_counter = 0;
                    }
                    pixel_counter_extract = pixel_counter_extract + 2;
                    if pixel_counter_extract == length_of_data_to_extract * 8 {
                        break;
                    }
                }   
                password_bit_counter = password_bit_counter + 1;
                if password_bit_counter == bits_for_password.len() {
                    password_bit_counter = 0;
                }             
            }
            if pixel_counter_extract == (length_of_data_to_extract * 8) && length_of_data_to_extract != 0 {
                break;
            }
        }   
        let bytes_extracted: Vec<char> = get_chars_from_bits(&bits_to_extract);   
        
        let mut new_output_file_name = output_file;
        if output_file.len()==0 {
            new_output_file_name = &original_file_name;
        }
        let mut output_file_file = File::create(&new_output_file_name).unwrap();
        let bytes: Vec<u8> = bytes_extracted.iter().map(|c| *c as u8).collect();
        if output_file_file.write_all(&bytes).is_err() {
            println!("Could not write to output file (-o): {}", output_file);
            std::process::exit(1)
        }
    }
}

mod create {
    use image::{GenericImage, GenericImageView, ImageBuffer, RgbImage, ImageOutputFormat};
    use std::ops::BitXor;    
    
    fn get_new_pixel_channel_value(b: bool, current_value: u8) -> u8
    {
        let mut return_value = 0;
        if b == true { //If true then make sure new number is an even number
            if current_value % 2 == 0 {
                return_value = current_value;
            } else {
                if current_value == 255 {
                    return_value = 254;
                } else if current_value <= 1 {
                    return_value = 2;
                } else {
                    return_value = current_value - 1;
                }
            }
        } else { //If false then make sure new number is an odd number
            if current_value % 2 != 0 {
                return_value = current_value;
            } else {
                if current_value == 254 {
                    return_value = 255;
                } else if current_value <= 1 {
                    return_value = 1;
                } else {
                    return_value = current_value - 1;
                }
            }
        }   
        // print!("{}", {if return_value % 2 == 0 {1} else {0}});
        // use std::io::Write;
        // std::io::stdout().flush();

        return_value
    }

    pub fn generate_output(data_to_hide: &[u8], input_file_name_with_data_to_hide: &str, input_file_name: &str, output_file: &str, password: &Vec<u8>) {
        let bits_to_hide_as_boolean: Vec<bool> = super::extract_bits(&data_to_hide);
        //println!("Bits: {}", bits_to_hide.len().to_string());
        let mut bits_to_hide_length: Vec<bool> = super::extract_bits(&data_to_hide.len().to_string().as_bytes());
        let mut bits_to_hide_file_name: Vec<bool> = super::extract_bits(&input_file_name_with_data_to_hide.as_bytes());
        let mut bits_for_password: Vec<bool> = super::extract_bits(&password);

        // print!("Password: ");
        // for b in &bits_for_password {
        //     print!("{}", {if *b {1} else {0}});
        //     use std::io::Write;
        //     std::io::stdout().flush();
        // }

        //let mut bits_to_hide_length: Vec<bool> = extract_bits(&data_to_hide);
        let mut length_bit_counter = 0;
        loop{
            if bits_to_hide_length.len()==super::MAX_LENGTH_DATA as usize {
                break;
            }
            else {
                bits_to_hide_length.push(false)
            }
        } 
        length_bit_counter = 0;
        loop{
            if bits_to_hide_file_name.len()==super::MAX_LENGTH_FILE_NAME as usize {
                break;
            }
            else {
                bits_to_hide_file_name.push(false)
            }
        } 
                
        
        let mut image_output = image::io::Reader::open(input_file_name).unwrap().with_guessed_format().unwrap().decode().unwrap();
        let space_available = (((image_output.dimensions().0 * image_output.dimensions().1)*4)/16) - 1024;
        
        if space_available < data_to_hide.len() as u32 {
            println!("Available space: {}", space_available);
            println!("Space required: {}", data_to_hide.len());
            println!("Cannot hide so much data in provided input file! (-i)");
            std::process::exit(1);
        }


        //let color_type = image_output.color();
        //println!("color type: {:?}", color_type);
        //let mut new_image = image.to_rgb();        
        let mut bit_counter = 0;
        let mut password_bit_counter: usize = 0;
        let mut stop_encoding_secret: bool = false;
        for x in (0..(image_output.dimensions().0 -1)).step_by(1) {
            for y in (0..(image_output.dimensions().1 -1)).step_by(1) {    
                let pixel = image_output.get_pixel(x, y);                

                if length_bit_counter < super::MAX_LENGTH_DATA { //Data length
                    let new_pixel_value: u8 = get_new_pixel_channel_value(bits_to_hide_length[length_bit_counter as usize] ^ bits_for_password[password_bit_counter], pixel[2]);                                    
                    image_output.put_pixel(x, y , image::Rgba([pixel[0], pixel[1], new_pixel_value, pixel[3]]));                     
                    length_bit_counter = length_bit_counter + 1;
                } else if length_bit_counter < super::MAX_LENGTH_DATA + super::MAX_LENGTH_FILE_NAME { //File name
                    let new_pixel_value: u8 = get_new_pixel_channel_value(bits_to_hide_file_name[(length_bit_counter - super::MAX_LENGTH_DATA) as usize] ^ bits_for_password[password_bit_counter], pixel[2]);                                    
                    image_output.put_pixel(x, y , image::Rgba([pixel[0], pixel[1], new_pixel_value, pixel[3]])); 
                    length_bit_counter = length_bit_counter + 1;
                } else if stop_encoding_secret == false {
                    let new_pixel_value_r: u8 = get_new_pixel_channel_value(bits_to_hide_as_boolean[bit_counter] ^ bits_for_password[password_bit_counter], pixel[0]);
                    let new_pixel_value_b: u8 = get_new_pixel_channel_value(bits_to_hide_as_boolean[bit_counter+1] ^ bits_for_password[password_bit_counter], pixel[2]);
                    image_output.put_pixel(x, y , image::Rgba([new_pixel_value_r, pixel[1], new_pixel_value_b, pixel[3]])); 
                    //image_output.put_pixel(x, y , image::Rgba([255,0,0,255])); 
                    bit_counter = bit_counter + 2;
                    if bit_counter == bits_to_hide_as_boolean.len()
                    {
                        stop_encoding_secret = true;
                    }
                } else if stop_encoding_secret {                    
                    let new_pixel_value_r: u8 = get_new_pixel_channel_value((pixel[0] % 2 == 0) ^ bits_for_password[password_bit_counter], pixel[0]);
                    let new_pixel_value_b: u8 = get_new_pixel_channel_value((pixel[2] % 2 == 0) ^ bits_for_password[password_bit_counter], pixel[2]);
                    image_output.put_pixel(x, y , image::Rgba([new_pixel_value_r, pixel[1], new_pixel_value_b, pixel[3]])); 
                    //println!("new_pixel_value_r {} {}", new_pixel_value_r, pixel[0]);
                }
                password_bit_counter = password_bit_counter + 1;
                if password_bit_counter == bits_for_password.len() {
                    password_bit_counter = 0;
                }
            }            
            //if bit_counter == bits_to_hide_as_boolean.len()
            //{
            //    break;
            //}
        }    
        //println!("{:?}", image::ImageOutputFormat::Jpeg(100));        
        image_output.save(output_file).unwrap();
    }
}